#!/usr/bin/env ruby
# encoding: UTF-8
##############################################
##         BACKUP SKRIPT BY STIWY           ##
##                   2015                   ##
##          version 0.1 - projects          ##
##         email: stefan@jarina.cz          ##
##############################################
require 'fileutils'
require 'sqlite3'

### COLORIZE OUTPUT ###
if $stdout.tty?
  def colorize(text, color_code)
    "\033[#{color_code}m#{text}\033[0m"
  end
  class String
    def red; colorize(self, "1;31;40"); end;
    def green; colorize(self, "1;32;40"); end;
    def darkgreen; colorize(self, "0;32;40"); end;
    def yellow; colorize(self, "1;33;40"); end;
    def darkcyan; colorize(self, "0;36;40"); end;
  end
else
  class String
    def red; self; end
    def green; self; end
    def darkgreen; self; end
    def yello; self; end
    def darkcyan; self; end
  end
end
### END ###

time = Time.now

backup_start = time.strftime("%Y-%m-%d %H:%M:%S")
puts "-".darkcyan * 26
puts "- BACKUP SCRIPT BY STIWY -".darkcyan
puts "-         #{time.year}           -".darkcyan
puts "-".darkcyan * 26
sleep 1

#### DUPLICITY SETUP START ####

puts "Configuring all the variables\n"
# PASSPHRASE used to encrypt backups:
# By default it picks up passphrase from env variable
# You can uncomment following line to set it up directly (unsecure as hell)
#ENV['PASSPHRASE'] = 'passphrase'

#Which program to use for backups - DUPLICITY:
if system("which duplicity >/dev/null 2>&1") == true
  backup_tool = `which duplicity`.chomp
else
  puts "Couldn't locate backup tool.\nScript will exit.".red
  abort
end

# WHAT TO BACK UP AND SPECIAL SETTINGS
back_what = '/admin/projects'
back_incl_list = Dir.pwd + "/file-list.txt"
special_settings = "--allow-source-mismatch"

# Setup of backup disk and names for directory for week
back_disk = "/media/disk"
back_dir = Time.now.strftime("%V").to_s

# Setup of destination directory
back_dest = back_disk + '/jarina_cz/backup_projects'
back_path = back_dest + '/' + back_dir

# Setup of retention
back_type = 'full' #FULL BACKUP - set later in script to once per week
max_full_backups = 12 #Number of weeks to keep backups
#### DUPLICITY SETUP STOP ####

#### BACKUP START ####
# Create and check folders for backups
if File.directory? back_path
  back_type = 'incr'
  puts "Backup type set to " + "INCREMENTAL".yellow
else
  begin
    Dir.mkdir(back_path)
  rescue Exception => e
    puts "Couldn't create backup directory: #{back_path}".red
    puts "Script will end now!".red
    puts e.message
    puts e.backtrace.inspect
    abort
  end
  back_type = 'full'
  puts "Backup type set to " + "FULL".yellow
end
sleep 1
puts ""

# Backup of directory
puts "-" * 30
puts "Backing up directory " + "#{back_what}".green + "\n"
begin
  prog_to_run = "#{backup_tool} #{back_type} --include-globbing-filelist #{back_incl_list} #{special_settings} #{back_what} file:///#{back_path}"
  out_back_files = `#{prog_to_run}`
rescue Exception => e
  puts "Couldn't backup files:".red
  puts e.message
  puts e.backtrace.inspect
  abort
end
puts out_back_files
puts "done".darkgreen
sleep 2
puts ""
#### BACKUP STOP ####

#### CLEAN AFTER BACKUP ####
# Calculate when clean
when_clean_calc = Time.now.strftime("%V").to_i - max_full_backups
if when_clean_calc < 10
  when_clean_calc = "0#{when_clean_calc}"
end

# Backup folder cleanup
puts "-" * 30
case max_full_backups
when 1
  puts "Removing backup of files older than 1 week"
else
  puts "Removing backup of files older than #{max_full_backups} weeks"
end
puts

dir_del = "#{back_path}/#{when_clean_calc}"
if File.directory? dir_del
  begin
    FileUtils.rm_rf(dir_del)
    puts "Removed #{dir_del}".darkgreen
  rescue Exception => e
    puts "Couldn't remove #{dir_del}".red
    puts e.message
  end
else
  puts "There is nothing to remove.".darkgreen
end
sleep 2
puts ""
#### CLEAN STOP ####

#### STATISTIC START ####
puts "-" * 30
puts "-" * 10 + "STATISTIC" + "-" * 11
puts "Start: " + "#{backup_start}".yellow
puts "End: " + "#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}".yellow
puts "Backup size of files in " + "projects/#{back_dir}".green + ": " + "#{`du -sh #{back_path}`}".yellow
puts "Total size of directory " + "projects".green + ": " + "#{`du -sh #{back_dest}`}".yellow
puts "-" * 12 + "SERVER" + "-" * 12
puts "Uptime: #{`uptime`}"
puts "-" * 30
puts "-" * 9 + "BACKUP DONE" + "-" * 10

#### SAVE RESULTS TO DATABASE ####
errors = out_back_files[-2].split[1].to_i
status = errors == 0 ? "SUCCESS" : "FAILURE"
begin
  db = SQLite3::Database.open "backups.db"
  db.execute "CREATE TABLE IF NOT EXISTS Backups(Id INTEGER PRIMARY KEY,
              Date TEXT, Status TEXT, Output TEXT, Errors INTEGER,
              Did_read INTEGER)"
  stmt = db.prepare "INSERT INTO Backups(Date, Status, Output, Errors, Did_read)
                    VALUES(?, ?, ?, ?, 0)"
  stmt.bind_params("#{Time.now}", status, out_back_files, errors)
  stmt.execute
  puts "Results have been stored in database".green
rescue SQLite3::Exception => e
  puts "Problem with database occured".red
  puts e
ensure
  stmt.close if stmt
  db.close if db
end

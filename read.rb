#!/usr/bin/env ruby
require 'sqlite3'
require 'slop'

def list(opts, args)
    query = ""
    if opts.to_hash[:all]
        query = "SELECT Id, Date, Status FROM Backups"
    else
        query = "SELECT Id, Date, Status FROM Backups WHERE Did_read=0"
    end
    begin
        db = SQLite3::Database.open "backups.db"
        results = db.execute query
        results.each do |row|
            puts row.join " ----- "
        end
    rescue SQLite3::Exception => e
        puts "Problem with database occured"
        puts e
    ensure
        db.close if db
    end
end

def show(opts, args)
    if args[0] == nil
      puts help
      exit 1
    else
      id = args[0].to_i
    end
    begin
      db = SQLite3::Database.open "backups.db"
      db.results_as_hash = true
      results = db.execute "SELECT * FROM Backups WHERE Id=#{id}"
      results.each do |row|
        puts "Date: #{row['Date']}"
        puts "Status: #{row['Status']}"

        errors = row['Errors'].to_i > 0 ? row['Errors'] : "NO ERRORS"

        puts "Errors: #{errors}"
        output = row['Output'].split('\n')
        puts "Outpus:"
        puts output
        puts "Marked as read."
      end
      db.execute "UPDATE Backups SET Did_read=1 WHERE Id=#{id}"
    rescue SQLite3::Exception => e
      puts "Problem with database occured"
      puts e
    ensure
      db.close if db
    end
end

def clear()
    begin
      db = SQLite3::Database.open "backups.db"
      results = db.execute "SELECT Id FROM Backups WHERE Did_read=0"
      results.each do |id|
        db.execute "UPDATE Backups SET Did_read=1 WHERE Id=#{id[0]}"
        puts "Id: #{id} was marked as read."
      end
    rescue SQLite3::Exception => e
      puts "Problem with database occured"
      puts e
    ensure
      db.close if db
    end
end

opts = Slop.parse!(help: true) do
  banner 'Usage: read.rb <command> [options]'

  on :v, 'Print the current version' do
    puts "version: 0.1"
    exit(-1)
  end

  command 'list', help: true do
    banner 'Usage: read.rb list [options]'
    description 'Show list of unread backups'

    on :all, 'Show list of all backups'

    run do |opts, args|
      list(opts, args)
    end

  end

  command 'show', help: true do
    banner 'Usage: read.rb show <Id>'
    description 'Show backup details'

    run do |opts, args|
      show(opts, args)
    end
  end

  command 'clear' do
    banner 'Usage: read.rb clear'
    description 'Clear all unread backups'
    run do
      clear
    end
  end

  run do
    puts help
  end
end
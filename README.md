# projects_backup
Just my simple script to backup my projects folder with retention and encryption (GnuPG) using 'duplicity' as backup tool

**I'm using disk mounted to system via SAMBA and tool DUPLICITY**

## DISCLAIMER
I created this script for my personal needs and am still actively working on it.

##### TODO
- [x] color output when on tty
- [x] simple database support
- [x] simple way to read backup information from database
- [ ] parse duplicity output
- [ ] complex db support with weekly/monthly reports
- [ ] rework to be more general and not tied to my needs :-)

## HOW IT WORKS
`backup.rb` --- backup script
* first day in week will create full backup and next days just incremental ones
* backup are put in week-numbered folder (week number of current year)
* 12 weeks old backups will be removed

`file-list.txt` --- excludes list
* here you can specify which folder you wanna exclude from or include to backup
* More information about this file on [duplicity web page](http://duplicity.nongnu.org/duplicity.1.html#sect9)

Script will back up everything except of params in file-list.txt

### SETUP


```shell
apt-get install duplicity
yum install duplicity

git clone https://gitlab.com/stefanjarina/projects_backup.git ~/scripts/projects_backup
cd ~/scripts/projects_backup
bundle install
chmod u+x backup.rb read.rb
```

You can add it to your crontab
`crontab -e`

```crontab
MAILTO=""
0 0,4,8,10,12,14,16,18,20,21,22 * * * time /home/admin/scripts/projects_backup/backup.rb | mailx -s "projects - backup" -a 'Content-Type: text/plain; charset=UTF-8' john@doe.com
```

##### Example of output
```
--------------------------
- BACKUP SCRIPT BY STIWY -
-         2015           -
--------------------------
Configuring all the variables
Backup type set to INCREMENTAL

------------------------------
Backing up directory /admin/projects
Reading globbing filelist /admin/scripts/projects_backup/file-list.txt
Local and Remote metadata are synchronized, no sync needed.
Last full backup date: Tue Dec 29 13:49:52 2015
--------------[ Backup Statistics ]--------------
StartTime 1451402724.40 (Tue Dec 29 16:25:24 2015)
EndTime 1451402744.54 (Tue Dec 29 16:25:44 2015)
ElapsedTime 20.14 (20.14 seconds)
SourceFiles 24781
SourceFileSize 808345026 (771 MB)
NewFiles 0
NewFileSize 0 (0 bytes)
DeletedFiles 0
ChangedFiles 0
ChangedFileSize 0 (0 bytes)
ChangedDeltaSize 0 (0 bytes)
DeltaEntries 0
RawDeltaSize 0 (0 bytes)
TotalDestinationSizeChange 104 (104 bytes)
Errors 0
-------------------------------------------------

done

------------------------------
Removing backup of files older than 12 weeks

There is nothing to remove.

------------------------------
----------STATISTIC-----------
Start: 2015-12-29 16:25:21
End: 2015-12-29 16:25:48
Backup size of files in projects/53: 125M       /media/disk/backups/backup_projects/53

Total size of directory projects: 125M  /media/disk/backups/backup_projects

------------SERVER------------
Uptime:  16:25:49 up 76 days, 18:56,  0 users,  load average: 0.64, 0.27, 0.19
------------------------------
---------BACKUP DONE----------
Results have been stored in database
```